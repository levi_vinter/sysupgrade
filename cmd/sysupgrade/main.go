package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/levi_vinter/sysupgrade/internal"
)

func main() {
	log.SetFlags(0)

	upgrade := flag.Bool("upgrade", false, "Upgrade all outdated packages")
	flag.Parse()

	if *upgrade {
		sysupgrade.Upgrade()

		os.Exit(0)
	}

	sysupgrade.CheckOutdatedPackages()
}
