package pipx

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os/exec"
	"strings"
	"sync"
)

type PipxList struct {
	Venvs map[string]interface{} `json:"venvs"`
}

func listPipxPackages() ([]string, error) {
	var packages []string
	output, err := exec.Command(
		"pipx", "list", "--json",
	).Output()
	if err != nil {
		errMessage := fmt.Sprintf("pipx list --json: %s", output)
		return packages, errors.New(errMessage)
	}

	var pipxlist PipxList

	err = json.Unmarshal(output, &pipxlist)
	if err != nil {
		return packages, errors.New(err.Error())
	}

	for packageName := range pipxlist.Venvs {
		packages = append(packages, packageName)
	}

	return packages, nil
}

func packageIsOutdated(packageName string) (bool, error) {
	pipOutdatedOutput, err := exec.Command(
		"pipx", "runpip", packageName, "list", "--outdated", "--format", "freeze",
	).Output()
	if err != nil {
		errMessage := fmt.Sprintf(
			"pipx runpip "+packageName+" list --outdated --format freeze: :%s",
			pipOutdatedOutput,
		)
		return false, errors.New(errMessage)
	}

	pipOutdatedOutputLines := strings.Split(
		strings.TrimRight(string(pipOutdatedOutput), "\n"),
		"\n",
	)
	for _, outdatedPackages := range pipOutdatedOutputLines {
		outdatedPackage := strings.Split(outdatedPackages, "==")[0]
		if outdatedPackage == packageName {
			return true, nil
		}
	}

	return false, nil
}

// Thread safe package name slice
type outdatedPackageHandler struct {
	outdatedPackages []string
	mu               sync.Mutex
}

func (p *outdatedPackageHandler) append(packageName string) {
	p.mu.Lock()
	defer p.mu.Unlock()

	p.outdatedPackages = append(p.outdatedPackages, packageName)
}

func GetPythonPackagesToUpgrade() ([]string, error) {
	packageHandler := outdatedPackageHandler{}
	pythonPackagesToCheck, err := listPipxPackages()
	if err != nil {
		return pythonPackagesToCheck, err
	}
	var isOutdated bool
	var wg sync.WaitGroup
	for _, packageName := range pythonPackagesToCheck {
		wg.Add(1)
		go func(packageName string) {
			defer wg.Done()
			isOutdated, err = packageIsOutdated(packageName)
			if isOutdated {
				packageHandler.append(packageName)
			}
		}(packageName)
	}

	wg.Wait()

	return packageHandler.outdatedPackages, nil
}

func UpgradeOutdatedPackages() {
	packageNames, err := GetPythonPackagesToUpgrade()
	if len(packageNames) == 0 {
		return
	}
	var output []byte
	for _, packageName := range packageNames {
		log.Printf("pipx upgrade %s\n", packageName)
		output, err = exec.Command("pipx", "upgrade", packageName).Output()
		if err != nil {
			log.Printf("pipx upgrade %s: %s\n", packageName, output)
		}
	}
}
