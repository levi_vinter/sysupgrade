package npm

import (
	"log"
	"os/exec"
	"strings"
)

func parsePackageNamesFromNpmOutput(outputLines []string) []string {
	var packageNames []string
	var packageName string
	for _, outputLine := range outputLines {
		if strings.HasPrefix(outputLine, "Package") {
			continue
		}
		packageName = strings.Split(outputLine, " ")[0]
		if packageName == "" {
			continue
		}
		packageNames = append(packageNames, packageName)
	}

	return packageNames
}

func getLocalPackagesToUpgrade() []string {
	// ignore error since npm outdated always returns with exit code 1
	npmOutput, _ := exec.Command("npm", "outdated").Output()
	outputLines := strings.Split(
		strings.TrimRight(string(npmOutput), "\n"),
		"\n",
	)

	outdatedPackages := parsePackageNamesFromNpmOutput(outputLines)

	return outdatedPackages

}

func getGlobalPackagesToUpgrade() []string {
	// ignore error since npm outdated always returns with exit code 1
	npmOutput, _ := exec.Command("npm", "outdated", "-g").Output()
	outputLines := strings.Split(
		strings.TrimRight(string(npmOutput), "\n"),
		"\n",
	)

	outdatedPackages := parsePackageNamesFromNpmOutput(outputLines)

	return outdatedPackages

}

func GetNpmPackagesToUpgrade() []string {
	var packagesToUpgrade []string
	localPackagesToUpgrade := getLocalPackagesToUpgrade()
	globalPackagesToUpgrade := getGlobalPackagesToUpgrade()
	if len(localPackagesToUpgrade) > 0 {
		packagesToUpgrade = append(packagesToUpgrade, localPackagesToUpgrade...)
	}

	if len(globalPackagesToUpgrade) > 0 {
		packagesToUpgrade = append(packagesToUpgrade, globalPackagesToUpgrade...)
	}

	return packagesToUpgrade
}

func UpgradeOutdatedPackages() {
	localPackageNames := getLocalPackagesToUpgrade()
	for _, packageName := range localPackageNames {
		log.Printf("npm update %s\n", packageName)
		output, err := exec.Command("npm", "update", packageName).Output()
		if err != nil {
			log.Printf("error: npm update %s: %s", packageName, output)
		}
	}
	globalPackageNames := getGlobalPackagesToUpgrade()
	for _, packageName := range globalPackageNames {
		log.Printf("npm update -g %s\n", packageName)
		output, err := exec.Command("npm", "update", "-g", packageName).Output()
		if err != nil {
			log.Printf("error: npm update -g %s: %s", packageName, output)
		}
	}
}
