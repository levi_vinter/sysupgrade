package flatpak

import (
	"log"
	"os/exec"
	"strings"
)

func GetFlatpakPackagesToUpgrade() []string {
	flatpakOutput, _ := exec.Command("flatpak", "update").Output()

	outputLines := strings.Split(
		strings.TrimRight(string(flatpakOutput), "\n"),
		"\n",
	)

	var packagesToUpgrade []string
	var packageName string
	for _, outputLine := range outputLines {
		if !strings.HasPrefix(outputLine, "Required runtime for ") {
			continue
		}
		packageName = strings.Split(outputLine, " ")[3]
		if strings.Contains(packageName, "/") {
			packageName = strings.Split(packageName, "/")[0]
		}
		packagesToUpgrade = append(packagesToUpgrade, packageName)
	}

	return packagesToUpgrade
}

func UpgradeOutdatedPackages() {
	packagesToUpgrade := GetFlatpakPackagesToUpgrade()
	for _, packageName := range packagesToUpgrade {
		log.Printf("flatpak update %s -y", packageName)
		output, err := exec.Command(
			"flatpak", "update", packageName, "-y",
		).Output()
		if err != nil {
			log.Printf("error: flatpak update %s -y: %s", packageName, output)
		}
	}
}
