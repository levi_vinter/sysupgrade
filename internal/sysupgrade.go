package sysupgrade

import (
	"fmt"
	"log"
	"os/user"
	"strings"
	"sync"

	"gitlab.com/levi_vinter/sysupgrade/internal/flatpak"
	"gitlab.com/levi_vinter/sysupgrade/internal/npm"
	"gitlab.com/levi_vinter/sysupgrade/internal/pipx"
)

func checkIfRoot() {
	user, err := user.Current()
	if err != nil {
		errMessage := fmt.Sprintf("current user: %s", err.Error())
		log.Fatalln(errMessage)
	}

	username := user.Username
	if username != "root" {
		log.Fatalln("you must be root")
	}
}

func printPythonPackagesToUpgrade(wg *sync.WaitGroup) {
	defer wg.Done()
	pythonPackagesToUpgrade, err := pipx.GetPythonPackagesToUpgrade()
	if err != nil {
		log.Println(err.Error())
		return
	}
	if len(pythonPackagesToUpgrade) == 0 {
		log.Println("no python packages to upgrade")
		return
	}

	log.Printf(
		"python packages to upgrade: %s",
		strings.Join(pythonPackagesToUpgrade, ", "),
	)
}

func printFlatpakPackagesToUpgrade(wg *sync.WaitGroup) {
	defer wg.Done()
	flatpakPackages := flatpak.GetFlatpakPackagesToUpgrade()
	if len(flatpakPackages) == 0 {
		log.Println("no flatpak packages to upgrade")
		return
	}

	log.Println(
		"flatpak packages to upgrade:",
		strings.Join(flatpakPackages, ", "),
	)
}

func printNpmPackagesToUpgrade(wg *sync.WaitGroup) {
	defer wg.Done()
	npmPackages := npm.GetNpmPackagesToUpgrade()

	if len(npmPackages) == 0 {
		log.Println("no npm packages to upgrade")
		return
	}

	log.Printf(
		"npm packages to upgrade: %s",
		strings.Join(npmPackages, ", "),
	)
}

func CheckOutdatedPackages() {
	var wg sync.WaitGroup
	wg.Add(3)

	go printPythonPackagesToUpgrade(&wg)
	go printFlatpakPackagesToUpgrade(&wg)
	go printNpmPackagesToUpgrade(&wg)

	wg.Wait()
}

func Upgrade() {
	checkIfRoot()

	var wg sync.WaitGroup
	wg.Add(3)

	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		pipx.UpgradeOutdatedPackages()
	}(&wg)

	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		npm.UpgradeOutdatedPackages()
	}(&wg)

	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		flatpak.UpgradeOutdatedPackages()
	}(&wg)

	wg.Wait()
}
